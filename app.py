from flask import Flask
from config import config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mail import Mail

app = Flask(__name__)
#config
app.config.from_object(config)
#email
Email = Mail(app)
#database
db = SQLAlchemy(app)
#blue print
from site_admin import admin
from site_user import user
from site_musics import musics
#admin
app.register_blueprint(admin)
#users
app.register_blueprint(user)
#music
app.register_blueprint(musics)

migrate = Migrate(app , db)
